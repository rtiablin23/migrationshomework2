﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Migrations2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations2
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<DentalCard> DentalCards { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Teeth> Teeths { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\User\Desktop\Project\GitRepositories\MigrationsHomeWork2\Migrations2\Migrations2");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<User>()
                .HasIndex(x => x.Login)
                .IsUnique();
        }
    }
}
