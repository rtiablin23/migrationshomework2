﻿using Migrations2.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations2.Models
{
    public class DentalCard : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Visit")]
        public int VisitId { get; set; }

        [ForeignKey("Teeth")]
        public int TeethId { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 1)]
        public string Information { get; set; }

        [StringLength(1000, MinimumLength = 1)]
        public string Commit { get; set; }

        public virtual Visit Visit { get; set; }
        public virtual Teeth Teeth { get; set; }
    }
}
