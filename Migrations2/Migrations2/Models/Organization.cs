﻿using Migrations2.Enums;
using Migrations2.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations2.Models
{
    public class Organization : IEntity<int>
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200, MinimumLength =1)]
        public string Name { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Address { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Supervisor { get; set; }
    }
}
