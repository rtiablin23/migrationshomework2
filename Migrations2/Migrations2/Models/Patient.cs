﻿using Migrations2.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations2.Models
{
    public class Patient : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }


        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Surname { get; set; }
        public string Patronymic { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public char Sex { get; set; }
        public DateTime CreateAccDate { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
