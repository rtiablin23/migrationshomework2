﻿using Migrations2.Enums;
using Migrations2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations2.Models
{
    public class Teeth : IEntity<int>
    {
        public int Id { get; set; }
        public JawType JawType { get; set; }
        public JawSide JawSide { get; set; }
        public int ToothNumber { get; set; }
    }
}
