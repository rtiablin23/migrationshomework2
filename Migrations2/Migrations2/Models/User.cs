﻿using Migrations2.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations2.Models
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Login { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string FullName { get; set; }

        [Required]
        [StringLength(500, MinimumLength = 1)]
        public string Password { get; set; }

        [Required]
        public bool IsBlocked { get; set; }

        [Required]
        public DateTime AccDateCreate { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
