﻿using Migrations2.Enums;
using Migrations2.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations2.Models
{
    public class Visit : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        [Required]
        public VisitType VisitType { get; set; }

        [Required]
        public DateTime VisitDate { get; set; }

        [Required]
        public VisitStatus VisitStatus { get; set; }
        public decimal Price { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual User User { get; set; }
    }
}
